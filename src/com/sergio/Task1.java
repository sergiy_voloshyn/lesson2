package com.sergio;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        // write your code here
///1
        /*1. Ввести с консоли три числа, подсчитать сумму квадратов двух наибольших чисел.
         Результат вывести в консоль. */
        //input data
        Scanner in = new Scanner(System.in);
        int[] nums = new int[3];


        for (int i = 0; i < 3; i++) {
            System.out.println("Введите число " + (i + 1) + ":");
            nums[i] = Integer.parseInt(in.nextLine());
        }

        //simple sort
        int temp;

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length - 1; j++) {

                if (nums[j] > nums[j + 1]) {
                    temp = nums[j + 1];
                    nums[j + 1] = nums[j];
                    nums[j] = temp;
                }

            }

        }
        //print result
        int max1 = nums[1];
        int max2 = nums[2];

        System.out.println("Результат:" + (Math.pow(max1, 2) + Math.pow(max2, 2)));
    }
}
