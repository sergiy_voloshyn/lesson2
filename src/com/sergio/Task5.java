package com.sergio;

import java.util.Scanner;

public class Task5 {
    //5* Ввести n строк с консоли. Упорядочить и вывести строки в порядке возрастания их размера (кол-во символов). Если есть строки одинаковой длины - упорядочить их по алфавиту.

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);

        //количество строк
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());


        String[] arrayOfRows = new String[numRows];

        //Введите строку номер
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        //quick sort by length
        String temp;

        for (int i = arrayOfRows.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                //swap
                if (arrayOfRows[j].length() > arrayOfRows[j + 1].length()) {
                    temp = arrayOfRows[j + 1];
                    arrayOfRows[j + 1] = arrayOfRows[j];
                    arrayOfRows[j] = temp;
                }
            }
        }
        // qsort by alfabet

        /*(name1.compareTo(name2)
        нулевое значение, если строки равны,
        целое отрицательное число, если первая строка предшествует второй
        целое положительное число, если  первая строка следует за второй   */

        int c1;
        for (int i = arrayOfRows.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                //swap
                if (arrayOfRows[j].length() == arrayOfRows[j + 1].length()) {
                    c1 = arrayOfRows[j].compareTo(arrayOfRows[j + 1]);
                    if (c1 > 0) {
                        temp = arrayOfRows[j + 1];
                        arrayOfRows[j + 1] = arrayOfRows[j];
                        arrayOfRows[j] = temp;
                    }
                }
            }
        }


        for (int i = 0; i < numRows; i++) {
            System.out.println("Строка : " + arrayOfRows[i]);
        }
    }

}
