package com.sergio;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
///2
        /*
        2. Ввести с консоли три числа (a, b, c). Решить квадратное уравнение
        ax^2 + bx + c = 0. Результат вывести в консоль.
        */

        Scanner in = new Scanner(System.in);

        int[] nums2 = new int[3];


        for (int i = 0; i < 3; i++) {
            System.out.println("Введите число " + (i + 1) + ":");
            nums2[i] = Integer.parseInt(in.nextLine());
        }
        int a = nums2[0];
        int b = nums2[1];
        int c = nums2[2];
        /*дискриминант —  D = b*b − 4ac
        Если D < 0, корней нет;
        Если D = 0, есть ровно один корень;
        Если D > 0, корней будет два.*/
        double diskriminant = b * b - 4 * a * c;
        double x1;
        double x2;

        if (diskriminant < 0) {
            System.out.println("Корней нет");
        } else if (diskriminant == 0) {
            //x1=(-b+sqrt(D))/2a
            x1 = (-b / 2 * a);
            System.out.println("Корень Х1= " + x1);
        } else if (diskriminant > 0) {
            //x1=(-b+sqrt(D))/2a
            //x2=(-b-sqrt(D))/2a
            x1 = (-b + Math.sqrt(diskriminant)) / 2 * a;
            x2 = (-b - Math.sqrt(diskriminant)) / 2 * a;
            System.out.println("Корень Х1= " + x1);
            System.out.println("Корень Х2= " + x2);

        }
    }

}
