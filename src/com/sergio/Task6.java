package com.sergio;

import java.util.Scanner;

public class Task6 {
    //6* Ввести с консоли несколько предложений. Удалить в них все слова заданной длины, начинающиеся на согласную букву.
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);

        String sentence;

        //Введите строку
        System.out.println("Введите строку : ");
        sentence = scan.nextLine();


        //заданная длина слова
        System.out.println("Введите длину слова: ");
        int lengthWord = Integer.parseInt(scan.nextLine());
        //разбить на массив
        String words[] = sentence.split(" ");
        //согласные
        String soglEng = "qwrtpsdfghklzxcvbnm";
        String soglEngCap = "QWRTPSDFGHJKLZXCVBNM";

        String soglRus = "цкнгшщзхфвпрлджчсмтб";
        String soglRusCap = "ЦКНГШЩЗХФВПРЛДЖЧСМТБ";

        for (int i = 0; i < words.length; i++) {
            //найти согласную
            String str = words[i];
            int s1 = soglEng.indexOf(str.charAt(0));
            int s2 = soglEngCap.indexOf(str.charAt(0));
            int s3 = soglRus.indexOf(str.charAt(0));
            int s4 = soglRusCap.indexOf(str.charAt(0));

            if ((words[i].length() == lengthWord) & ((s1 > 0) | (s2 > 0) | (s3 > 0) | (s4 > 0))) {
                words[i] = "";
            }
        }
        //Результат

        System.out.println("Результат:");

        for (int i = 0; i < words.length; i++) {

            System.out.print(words[i] + " ");

        }


    }
}
