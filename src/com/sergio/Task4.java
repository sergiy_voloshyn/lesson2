package com.sergio;

import java.util.Scanner;

public class Task4 {
    //4. Ввести n строк с консоли. Вывести на консоль те строки, длина которых меньше средней.

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);

        //количество строк
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());


        String[] arrayOfRows = new String[numRows];

        //Введите строку номер
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        //найти среднюю длину строки
        int average = 0;
        for (int i = 0; i < numRows; i++) {
            average += arrayOfRows[i].length();
        }
        average = average / numRows;
        System.out.println("Средняя строка : " + average + " символов");

        //вывод строк меньше средней длины
        for (int i = 0; i < numRows; i++) {
            if (arrayOfRows[i].length() < average) {
                System.out.println("Строка меньше средней: " + arrayOfRows[i]);
            }

        }

    }

}
