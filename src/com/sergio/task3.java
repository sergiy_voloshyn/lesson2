package com.sergio;

import java.util.Scanner;

public class Task3 {


    //3. Ввести n строк с консоли, найти самую короткую строку. Вывести эту строку и ее длину.


    public static void main(String[] args) {

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter number of rows
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());

        //init arrays for saving rows
        String[] arrayOfRows = new String[numRows];

        //enter and save rows
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        //init variables for saving row with min length
        int minLength = arrayOfRows[0].length();
        String minLengthRow = arrayOfRows[0];

        //cycle for finding row with min length
        for (int i = 0; i < numRows; i++) {
            if (minLength > arrayOfRows[i].length()) {
                minLength = arrayOfRows[i].length();
                minLengthRow = arrayOfRows[i];
            }
        }

        //print row with min length
        System.out.println("Самая маленькая строка: " + minLengthRow);
        System.out.println("Ее длина: " + minLength);
    }


}
